var fs = require('fs'),
    path = require('path');


function getDirectories(srcpath) {
    return fs.readdirSync(srcpath).filter(function(file) {
        return fs.statSync(path.join(srcpath, file)).isDirectory();
    });
}

function getFiles(srcpath) {
    return fs.readdirSync(srcpath).filter(function(file) {
        return fs.statSync(path.join(srcpath, file)).isFile();
    });
}

function generateTestsForFolder(srcpath) {
    describe(srcpath.split('/').pop(), function() {
        var files = getFiles(srcpath);
        for (var i = 0; i < files.length; i++) {
            describe(files[i], function () {
                require(path.join(srcpath, files[i]));
            });
        }
        var dirs = getDirectories(srcpath);
        for(i = 0; i < dirs.length; i++) {
            generateTestsForFolder(path.join(srcpath, dirs[i]));
        }
    });
}

generateTestsForFolder(__dirname);