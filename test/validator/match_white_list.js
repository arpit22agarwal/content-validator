rules = [
{
    "contentType" : {
        "Video":{
            "priority" : 5,
            "rules" : [
            {
                "url" : {
                    "mandatory" : true,
                    "match" : {
                        "whitelist" : [/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/]
                    }
                }
            }            
            ]
        }
    }
}
]

module.exports = rules;