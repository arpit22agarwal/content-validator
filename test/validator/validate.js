var should = require('chai').should();
var validator = require('../../validator');
var Validator = new validator();

describe('success config' , function() {
	it('validate match' , function(done) {
		var post = {
			contentType : 'Video' ,
			url : "https://www.youtube.com/watch?v=q6EpNVhC5nw"
		}
		Validator.validate(post , require('./match_white_list') , function(err , value) {
			if(err) {
				done('validation failed')
			} else {
				done()
			}
		})
	});
});


describe('error config' , function() {
	it('validate match' , function(done) {
		var post = {
			contentType : 'Video' ,
			url : "https://www.randomurl.com"
		}
		Validator.validate(post , './match_white_list' , function(err , value) {
			if(err) {
				done()
			} else {
				done('validation failed')
			}
		})
	})
})