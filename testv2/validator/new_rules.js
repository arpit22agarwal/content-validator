module.exports = [
	{
		"if" : {
			"any" : [
				{
					"type" : ["article"]
				},
			],
			"all" : [
				{
					"status" : ["inReview" , "draft"]
				},
				{
					"source" : "myntra.com"
				}
			]
		},
		"apply" : {
			"any" : [
				{
					"description" : {
						"mandatory" : true,
					}
				},
				{
					"title" : {
						"length" : {
							"min" : 10,
							"max" : 160
						}
					}
				}
			],
			"all" : [
				{
					"url" : {
						"mandatory" : true,
						"match" : {
							"whitelist" : ["myntra.com"]
						}
					}
				},
				{
					"quote" : {
						"mandatory" : true
					}
				}
			]
		}
	}
]