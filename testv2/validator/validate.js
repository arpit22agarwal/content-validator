var should = require('chai').should();
var validator = require('../../validator');
var Validator = new validator();

describe('success config' , function() {
	it('validate match' , function(done) {
		var post = {
			type : 'article' ,
			url : "http://www.myntra.com/89765",
			status : "inReview",
			quote : "testing the rules",
			title : "Myntra - Online fashion App",
			source : "myntra.com"
		}
		Validator.validate(post , require('./new_rules') , function(err , value) {
			if(Validator._rules.length === 0) {
				done('rules not loaded');
			} else if(err) {
				done('validation failed');
			} else {
				done();
			}
		})
	});
});

describe('error config' , function() {
	it('validate match' , function(done) {
		var post = {
			type : 'article' ,
			url : "http://www.myntra.com/89765",
			status : "inReview",
			quote : "testing the rules",
			title : "App",
			source : "myntra.com"
		}
		Validator.validate(post , require('./new_rules') , function(err , value) {
			if(Validator._rules.length === 0) {
				done('rules not loaded');
			} else if(err) {
				done()
			} else {
				done('validation failed')
			}
		})
	});
});

