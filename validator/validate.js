var async = require('async');
var hashmap = require('hashmap');

// Define a class for validation
function validator() {
	this._rules = [];
}

// Get priority from config - internal use by the class
validator.prototype.getPriority = function(rules, rule, priority) {
	RetValue = 0
	if(rules[rule].priority) {
		RetValue = rules[rule].priority;
	}
	else if(priority) {
		RetValue = priority;
	}
	return RetValue;
}

// Extract Rules - internal use by class
validator.prototype.extractRules = function(category, map) {
	var that = this;
	if(category["priority"]) {
		priority = category["priority"];
	}
	if(category["rules"]) {
		category["rules"].forEach(function(rules) {
			for (var rule in rules) {
				rules[rule].priority = that.getPriority(rules, rule, priority)
				if(!map.has(rule) || map.get(rule)[rule]["priority"] < rules[rule].priority) {
					map.set(rule, rules);
				}
			}
		});
	}
}

// Read the passed config and store the required data in class member variable
validator.prototype.configure = function(obj, validationRules , callback) {
	var that = this;
	// Parse the config, and store the rules to be executed in a Hashmap
	// In case of conflicting rules, rule with highest priority is added.
	var map = new hashmap();
	validationRules.forEach(function(categoryRules) {
		var priority = null
		for ( var category in categoryRules) {
			// Global rule
			if(category == "global") {
				that.extractRules(categoryRules[category], map);
			}
			else {
				// Specific rules based on some field
				for(var fieldType in categoryRules[category]) {
					if(fieldType ==  obj[category]) {
						that.extractRules(categoryRules[category][fieldType], map);
					}
					
				}
			}
		}
	});
	map.keys().forEach(function(key) {
		fields = map.get(key);
		for( var field in fields) {
			var name = field;
			if('undefined' != typeof fields[field]["mandatory"]) {
				that._rules.push(async.apply(validateMandatory, obj, fields[field]["mandatory"], name));
			}
			if('undefined' != typeof fields[field]["length"]) {
				that._rules.push(async.apply(validateLength, name, obj[name], fields[field].length.min, fields[field].length.max));
			}
			if('undefined' != typeof fields[field]["empty"]) {
				that._rules.push(async.apply(validateEmpty, name, fields[field]["empty"], obj[name]));
			}
			if('undefined' != typeof fields[field]["match"]) {
				that._rules.push(async.apply(match, name, fields[field]["match"], obj[name]))
			}
		}		
	});
}

// Configures the object and runs all rules in parallel. Exits as soon as any rule fails
validator.prototype.validate = function(post, fRules, callback) {
	try {
		this.configure(post, fRules);
		async.parallel(this._rules, callback);
	}
	catch(ex) {
		callback(ex, null)
	}
}

// Check the required data in not empty
var validateMandatory = function(obj, isMandatory, field, callback) {
	if(!isMandatory) {
		callback(null, null);
	}
	else if(undefined === obj[field]) {
		callback("Required field " + field + " is not present", null)
	}
	else {
		callback(null, null);
	}
}

//Check if the passed field is not empty
var validateEmpty = function(name, isEmptyAllowed, data, callback) {
	if(isEmptyAllowed) {
		callback(null, null);
		return;
	}
	if(!data) {
		  callback(name + " cannot be empty", null);
	}
	else if(Array.isArray(data)) {
		if(data.length > 0) {
			bEmpty = true;
			data.every(function(element) {
				if(element) {
					bEmpty = false;
					callback(null, null);
					return;
				}
			})
			if(bEmpty) {
				callback(name + " cannot be empty", null);
			}
		} else {
			callback(name + " cannot be empty", null);
		}
	}
	else {
		callback(null, null);
	}
}
// Validate length of the passed value based on min or max criteria provided
var validateLength = function(field, value, min, max, callback) {
	if(!value) {
		//if the value is undefined or null, return success here, as presence validation is separately done in mandatory
		return callback(null , null);
	}
	var nLength = value.length;
	status = null;
	if(! min && max) {
		if(nLength > max) {
			status = "max characters allowed for " + field + " is " + max;
		}
	}
	else if(!max && min) {
		if(nLength < min) {
			status = "Minimum length of " + min + " required for " + field;
		}
	}
	else if(max && min!=undefined) {
		if(nLength < min || nLength > max) {
			status = "Length criteria not met for " + field;
		}
	}
	callback(status, null);
}

var _match = function(value, vRegEx, defaultVal) {
	if(vRegEx.length <= 0) {
		return defaultVal;
	}
	return vRegEx.some(function(regex) {
		var _regex = new RegExp(regex);
		isMatched = _regex.test(value);
		return isMatched;	
	});
}

//Validate for blacklisted URLs
var match = function(name, conditions, value , callback) {
	vBlackList = [];
	vWhiteList = [];
	if(conditions) {
		if(conditions.blacklist) {
			vBlackList = conditions.blacklist;
		}
		if(conditions.whitelist) {
			vWhiteList = conditions.whitelist;
		}
	}
	if(!value) {
		return callback(null, null);
	}
	var isValid = !_match(value, vBlackList , false) && _match(value , vWhiteList , true);
	if(!isValid) {
		return callback('Invalid Value in ' + name , null);
	}
	return callback(null, value);
}

module.exports = validator
